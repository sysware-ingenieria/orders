package com.sysware_ingenieria.orders;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity implements AdaptadorFotos.EscuchaEventosClick {

    RecyclerView reciclador;
    LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       // anadirToolbar();
        reciclador = (RecyclerView) findViewById(R.id.reciclador);



        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            layoutManager = new GridLayoutManager(this, 2);
        else
            layoutManager = new LinearLayoutManager(this);


        reciclador.setLayoutManager(layoutManager);

        AdaptadorFotos adaptador = new AdaptadorFotos(this);
        reciclador.setAdapter(adaptador);


    }
    private void anadirToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actividad_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    public static final String EXTRA_POSICION = "com.sysware-ingenieria.orders.extra.POSICION";

    @Override
    public void onItemClick(AdaptadorFotos.ViewHolder holder, int posicion) {
        Intent intent = new Intent(this, ActividadDetalle.class);
        intent.putExtra(EXTRA_POSICION, posicion);
        startActivity(intent);
    }

}
