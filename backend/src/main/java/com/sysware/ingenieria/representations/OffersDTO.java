package com.sysware.ingenieria.representations;

/**
 * Created by luis on 6/04/17.
 */
public class OffersDTO {

    private  long id;
    private DataDTO data;
    private  Integer units;
    private  double offer_value;
    private  String percent;
    private  String state;
    private  Long start_date;
    private  Long end_date;

    public OffersDTO(long id, DataDTO data, Integer units, double offer_value, String percent, String state, Long start_date, Long end_date) {
        this.id = id;
        this.data = data;
        this.units = units;
        this.offer_value = offer_value;
        this.percent = percent;
        this.state = state;
        this.start_date = start_date;
        this.end_date = end_date;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public DataDTO getData() {
        return data;
    }

    public void setData(DataDTO data) {
        this.data = data;
    }

    public Integer getUnits() {
        return units;
    }

    public void setUnits(Integer units) {
        this.units = units;
    }

    public double getOffer_value() {
        return offer_value;
    }

    public void setOffer_value(double offer_value) {
        this.offer_value = offer_value;
    }

    public String getPercent() {
        return percent;
    }

    public void setPercent(String percent) {
        this.percent = percent;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getStart_date() {
        return start_date;
    }

    public void setStart_date(Long start_date) {
        this.start_date = start_date;
    }

    public Long getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Long end_date) {
        this.end_date = end_date;
    }
}
