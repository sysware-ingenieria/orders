package com.sysware.ingenieria.representations;

import com.sysware.ingenieria.entities.Categories;
import com.sysware.ingenieria.entities.Data;
import com.sysware.ingenieria.entities.Marks;

/**
 * Created by luis on 6/04/17.
 */
public class ViewProductsDTO {
    private Data data;
    private  double unit_value;
    private  String unit_type;
    private Marks marks;
    private Categories category;

    public ViewProductsDTO(Data data, double unit_value, String unit_type, Marks marks, Categories category) {
        this.data = data;
        this.unit_value = unit_value;
        this.unit_type = unit_type;
        this.marks = marks;
        this.category = category;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public double getUnit_value() {
        return unit_value;
    }

    public void setUnit_value(double unit_value) {
        this.unit_value = unit_value;
    }

    public String getUnit_type() {
        return unit_type;
    }

    public void setUnit_type(String unit_type) {
        this.unit_type = unit_type;
    }

    public Marks getMarks() {
        return marks;
    }

    public void setMarks(Marks marks) {
        this.marks = marks;
    }

    public Categories getCategory() {
        return category;
    }

    public void setCategory(Categories category) {
        this.category = category;
    }
}
