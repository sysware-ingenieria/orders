package com.sysware.ingenieria.representations;

/**
 * Created by luis on 6/04/17.
 */
public class CategoriesDTO {
    private DataDTO data;

    public CategoriesDTO(DataDTO data) {
        this.data = data;
    }

    public DataDTO getData() {
        return data;
    }
    public void setData(DataDTO data) {
        this.data = data;
    }

}
