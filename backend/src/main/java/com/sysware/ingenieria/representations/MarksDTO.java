package com.sysware.ingenieria.representations;

/**
 * Created by luis on 6/04/17.
 */
public class MarksDTO {
    private DataDTO data;

    public MarksDTO(Long id, DataDTO data) {

        this.data = data;
    }

    public DataDTO getData() {
        return data;
    }

    public void setData(DataDTO data) {
        this.data = data;
    }
}
