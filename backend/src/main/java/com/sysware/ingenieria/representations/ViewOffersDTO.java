package com.sysware.ingenieria.representations;

import com.sysware.ingenieria.entities.Data;

/**
 * Created by luis on 6/04/17.
 */
public class ViewOffersDTO {

    private  Data data;
    private  Integer units;
    private  double offer_value;
    private  float percent;
    private  Integer state;
    private  long start;
    private  long end;

    public ViewOffersDTO( Data data, Integer units, double offer_value, float percent, Integer state, long start, long end) {

        this.data = data;
        this.units = units;
        this.offer_value = offer_value;
        this.percent = percent;
        this.state = state;
        this.start = start;
        this.end = end;
    }



    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Integer getUnits() {
        return units;
    }

    public void setUnits(Integer units) {
        this.units = units;
    }

    public double getOffer_value() {
        return offer_value;
    }

    public void setOffer_value(double offer_value) {
        this.offer_value = offer_value;
    }

    public float getPercent() {
        return percent;
    }

    public void setPercent(float percent) {
        this.percent = percent;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getEnd() {
        return end;
    }

    public void setEnd(long end) {
        this.end = end;
    }
}
