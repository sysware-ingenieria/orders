package com.sysware.ingenieria.DAOs;

import com.sysware.ingenieria.entities.PersonsOrders;
import com.sysware.ingenieria.entities.Products;
import com.sysware.ingenieria.mappers.ProductsMapper;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

/**
 * Created by luis on 6/04/17.
 */
@RegisterMapper(ProductsMapper.class)
public interface ProductsDAO {

    @SqlQuery(" SELECT pr.id as id, pr.id_data AS id_data, da.name AS name, da.description AS description, pr.id_category, pr.id_mark,pr.unit_value, pr.unit_type  FROM products AS pr INNER JOIN data as da ON pr.id_data = da.id ")
    List<Products> PRODUCTS_LIST();
}
