package com.sysware.ingenieria.DAOs;

import com.sysware.ingenieria.entities.Marks;
import com.sysware.ingenieria.entities.Offers;
import com.sysware.ingenieria.mappers.OffersMapper;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

/**
 * Created by luis on 6/04/17.
 */
@RegisterMapper(OffersMapper.class)
public interface OffersDAO {

   @SqlQuery("SELECT of.id,of.id_data AS id_data,of.units, of.offer_value, of.percent,of.state, of.start_date,of.end_date, da.name,da.description FROM offers AS of INNER  JOIN data AS da ON of.id_data=da.id ")
   List<Offers> OFFERS_LIST();
}
