package com.sysware.ingenieria.DAOs;

import com.sysware.ingenieria.entities.Persons;
import com.sysware.ingenieria.entities.PersonsOrders;
import com.sysware.ingenieria.mappers.PersonsOrdersMapper;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

/**
 * Created by luis on 6/04/17.
 */
@RegisterMapper(PersonsOrdersMapper.class)
public interface PersonsOrdersDAO {

    @SqlQuery("SELECT pe_ord.id, pe_ord.id_person, pe_ord.id_order, pe_ord.status FROM persons_orders  AS pe_ord ")
    List<PersonsOrders> PERSONS_ORDERS_LIST();

}
