package com.sysware.ingenieria.DAOs;

import com.sysware.ingenieria.entities.Data;
import com.sysware.ingenieria.entities.Marks;
import com.sysware.ingenieria.mappers.MarksMapper;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

/**
 * Created by luis on 6/04/17.
 */
@RegisterMapper(MarksMapper.class)
public interface MarksDAO {

    @SqlQuery("SELECT ma.id, ma.id_data,da.name,da.description  FROM marks as ma INNER JOIN data AS da ON ma.id_data = da.id ")
    List<Marks> MARKS_LIST();

}
