package com.sysware.ingenieria.DAOs;

import com.sysware.ingenieria.entities.Categories;
import com.sysware.ingenieria.entities.Data;
import com.sysware.ingenieria.mappers.DataMapper;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

@RegisterMapper(DataMapper.class)
public interface DataDAO {

    @SqlQuery("SELECT da.id, da.name, da.description FROM  data AS da; ")
    List<Data> DATA_LIST();
}
