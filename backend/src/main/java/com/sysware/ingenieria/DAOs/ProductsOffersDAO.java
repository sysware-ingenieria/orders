package com.sysware.ingenieria.DAOs;

import com.sysware.ingenieria.entities.Products;
import com.sysware.ingenieria.entities.ProductsOffers;
import com.sysware.ingenieria.mappers.ProductsOffersMapper;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

/**
 * Created by luis on 6/04/17.
 */
@RegisterMapper(ProductsOffersMapper.class)
public interface ProductsOffersDAO {


    @SqlQuery(" SELECT pr_of.id, pr_of.id_product, pr_of.id_product FROM products_offers AS pr_of ")
    List<ProductsOffers> PRODUCTS_OFFERS_LIST();

}
