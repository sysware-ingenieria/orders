package com.sysware.ingenieria.DAOs;

import com.sysware.ingenieria.entities.Offers;
import com.sysware.ingenieria.entities.Orders;
import com.sysware.ingenieria.mappers.OrdersMapper;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

/**
 * Created by luis on 6/04/17.
 */
@RegisterMapper(OrdersMapper.class)
public interface OrdersDAO {

    @SqlQuery("SELECT ord.id, ord.id_offert, ord.number FROM orders AS ord;")
    List<Orders> ORDERS_LIST();
}
