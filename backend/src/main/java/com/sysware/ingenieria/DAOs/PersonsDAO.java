package com.sysware.ingenieria.DAOs;

import com.sysware.ingenieria.entities.Orders;
import com.sysware.ingenieria.entities.Persons;
import com.sysware.ingenieria.mappers.PersonsMapper;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

/**
 * Created by luis on 6/04/17.
 */
@RegisterMapper(PersonsMapper.class)
public interface PersonsDAO {

    @SqlQuery("SELECT pe.id,pe.first_name,pe.second_name, pe.first_last_name, pe.second_last_name FROM persons as pe ")
    List<Persons> PERSONS_LIST();
}
