package com.sysware.ingenieria.DAOs;

import com.sysware.ingenieria.entities.Categories;
import com.sysware.ingenieria.mappers.CategoriesMapper;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

/**
 * Created by luis on 6/04/17.
 */
@RegisterMapper(CategoriesMapper.class)
public interface CategoriesDAO {

    @SqlQuery("SELECT ca.id, ca.id_data, data.name, data.description FROM categories as ca INNER JOIN data ON ca.id_data = data.id ")
    List<Categories> CATEGORIES_LIST();


}
