package com.sysware.ingenieria.DAOs;

import com.sysware.ingenieria.entities.ProductsOffers;
import com.sysware.ingenieria.entities.ViewProducts;
import com.sysware.ingenieria.mappers.ViewOffersMapper;
import com.sysware.ingenieria.mappers.ViewProductsMapper;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

@RegisterMapper(ViewProductsMapper.class)
public interface ViewProductsDAO {


    @SqlQuery(" SELECT * FROM view_products; ")
    List<ViewProducts> PRODUCTS_OFFERS_LIST();

}
