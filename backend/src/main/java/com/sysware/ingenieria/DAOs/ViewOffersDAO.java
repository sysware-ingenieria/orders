package com.sysware.ingenieria.DAOs;

import com.sysware.ingenieria.entities.Data;
import com.sysware.ingenieria.entities.ProductsOffers;
import com.sysware.ingenieria.entities.ViewOffers;
import com.sysware.ingenieria.mappers.ViewOffersMapper;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;


@RegisterMapper(ViewOffersMapper.class)
public interface ViewOffersDAO {


    @SqlQuery(" SELECT * FROM view_offers; ")
    List<ViewOffers> PRODUCTS_OFFERS_LIST();

}
