package com.sysware.ingenieria.utils.security;


import java.util.Random;

public class CodeGenerator {
    private static double randomNumber() {
        return new Random().nextDouble();
    }

    //generamos un codigo de licencia a los usuarios para la app.
    public static String codeGenerate(int codeLength, boolean isDached) {
        String codeTrial = "";
        for (int i = 1; i <codeLength ; i++) {
            codeTrial=codeTrial+codeCharacterGenerate(randomNumber());
            if ( i%4==0 && i<codeLength && isDached){
                codeTrial=codeTrial+"-";
            }
        }
        return codeTrial;
    }

    public static char codeCharacterGenerate(double randomDouble){
        long ramdom36=(long) (randomDouble*36)+1;
        int modulator = 0;
        if (ramdom36>10){
            modulator=1;
        }
        long ascii36=47+ramdom36+(modulator*7);
        return (char) ascii36;
    }
}
