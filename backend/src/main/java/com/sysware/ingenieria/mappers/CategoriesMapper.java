package com.sysware.ingenieria.mappers;


import com.sysware.ingenieria.entities.Categories;
import com.sysware.ingenieria.entities.Data;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by luis on 6/04/17.
 */
public class CategoriesMapper implements ResultSetMapper<Categories> {

    @Override
    public Categories map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Categories(
                resultSet.getLong("id"),
                new Data(
                        resultSet.getLong("id_data"),
                        resultSet.getString("name"),
                        resultSet.getString("description")
                )
        ) ;
    }
}
