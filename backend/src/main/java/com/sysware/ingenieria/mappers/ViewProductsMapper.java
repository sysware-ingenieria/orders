package com.sysware.ingenieria.mappers;


import com.sysware.ingenieria.entities.Categories;
import com.sysware.ingenieria.entities.Data;
import com.sysware.ingenieria.entities.Marks;
import com.sysware.ingenieria.entities.ViewProducts;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ViewProductsMapper implements ResultSetMapper<ViewProducts>  {
    @Override
    public ViewProducts map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new ViewProducts(
                resultSet.getLong("id"),
                new Data(
                        resultSet.getLong("d_p_id"),
                        resultSet.getString("d_p_name"),
                        resultSet.getString("d_p_description")
                ),
                resultSet.getDouble("unit_value"),
                resultSet.getString("unit_type"),// tipo de unidad, caja, etc.
                new Marks(
                        resultSet.getLong("id_marks"),
                        new Data(
                                resultSet.getLong("d_m_id"),
                                resultSet.getString("d_m_name"),
                                resultSet.getString("d_m_description")
                        )
                ),
                new Categories(
                        resultSet.getLong("id_category"),
                        new Data(
                                resultSet.getLong("d_c_id"),
                                resultSet.getString("d_c_name"),
                                resultSet.getString("d_c_description")
                        )
                )
        );
    }
}
