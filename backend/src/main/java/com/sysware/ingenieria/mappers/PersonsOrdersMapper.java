package com.sysware.ingenieria.mappers;

import com.sysware.ingenieria.entities.PersonsOrders;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by luis on 6/04/17.
 */
public class PersonsOrdersMapper implements ResultSetMapper<PersonsOrders> {

    @Override
    public PersonsOrders map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new PersonsOrders(
                resultSet.getLong("id"),
                resultSet.getLong("id_person"),
                resultSet.getLong("id_order"),
                resultSet.getString("status")
        );
    }
}
