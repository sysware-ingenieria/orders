package com.sysware.ingenieria.mappers;


import com.sysware.ingenieria.entities.Data;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DataMapper implements ResultSetMapper<Data> {

    @Override
    public Data map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Data(
                resultSet.getLong("id"),
                resultSet.getString("name"),
                resultSet.getString("description")
        );
    }
}
