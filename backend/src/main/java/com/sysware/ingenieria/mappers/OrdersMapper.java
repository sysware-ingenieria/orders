package com.sysware.ingenieria.mappers;

import com.sysware.ingenieria.entities.Orders;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by luis on 6/04/17.
 */
public class OrdersMapper  implements ResultSetMapper<Orders> {
    @Override
    public Orders map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Orders(
                resultSet.getLong("id"),
                resultSet.getLong("id_offert"),
                resultSet.getInt("number")
        );
    }
}
