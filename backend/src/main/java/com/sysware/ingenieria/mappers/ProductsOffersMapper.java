package com.sysware.ingenieria.mappers;

import com.sysware.ingenieria.entities.ProductsOffers;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by luis on 6/04/17.
 */
public class ProductsOffersMapper implements ResultSetMapper<ProductsOffers> {
    @Override
    public ProductsOffers map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new ProductsOffers(
                resultSet.getLong("id"),
                resultSet.getLong("id_product"),
                resultSet.getLong("id_offer")
        );
    }
}
