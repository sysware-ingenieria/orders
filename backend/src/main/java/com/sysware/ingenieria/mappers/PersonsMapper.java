package com.sysware.ingenieria.mappers;

import com.sysware.ingenieria.entities.Persons;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by luis on 6/04/17.
 */
public class PersonsMapper  implements ResultSetMapper<Persons> {
    @Override
    public Persons map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Persons(
                resultSet.getLong("id"),
                resultSet.getString("first_name"),
                resultSet.getString("second_name"),
                resultSet.getString("first_last_name"),
                resultSet.getString("second_last_name")
        );
    }
}
