package com.sysware.ingenieria.mappers;

import com.sysware.ingenieria.entities.Data;
import com.sysware.ingenieria.entities.Products;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by luis on 6/04/17.
 */
public class ProductsMapper implements ResultSetMapper<Products> {

    @Override
    public Products map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Products(
                resultSet.getLong("id"),
                new Data(
                        resultSet.getLong("id_data"),
                        resultSet.getString("name"),
                        resultSet.getString("description")
                ),
                resultSet.getLong("id_category"),
                resultSet.getLong("id_mark"),
                resultSet.getDouble("unit_value"),
                resultSet.getString("unit_type")
        );
    }
}
