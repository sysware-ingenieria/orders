package com.sysware.ingenieria.mappers;

import com.sysware.ingenieria.entities.Data;
import com.sysware.ingenieria.entities.Offers;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by luis on 6/04/17.
 */
public class OffersMapper  implements ResultSetMapper<Offers>  {
   @Override
   public Offers map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
      return new Offers(
              resultSet.getLong("id"),
              new Data(
                      resultSet.getLong("id_data"),
                      resultSet.getString("name"),
                      resultSet.getString("description")
              ),
              resultSet.getInt("units"),
              resultSet.getDouble("offer_value"),
              resultSet.getFloat("percent"),
              resultSet.getInt("state"),
              resultSet.getTimestamp("start_date"),
              resultSet.getTimestamp("end_date")

      );
   }
}
