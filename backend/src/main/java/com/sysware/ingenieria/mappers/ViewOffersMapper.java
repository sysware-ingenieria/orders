package com.sysware.ingenieria.mappers;

import com.sysware.ingenieria.entities.Data;
import com.sysware.ingenieria.entities.ViewOffers;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by luis on 6/04/17.
 */
public class ViewOffersMapper  implements ResultSetMapper<ViewOffers> {
    @Override
    public ViewOffers map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new ViewOffers(
                resultSet.getLong("id"),
                new Data(
                        resultSet.getLong("id_data_offer"),
                        resultSet.getString("name_offer"),
                        resultSet.getString("description_offer")

                ),
                resultSet.getInt("units"),
                resultSet.getDouble("offer_value"),
                resultSet.getFloat("percent"),
                resultSet.getInt("state"),
                resultSet.getTimestamp("start"),
                resultSet.getTimestamp("end")
        );
    }
}
