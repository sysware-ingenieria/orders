package com.sysware.ingenieria.mappers;

import com.sysware.ingenieria.entities.Data;
import com.sysware.ingenieria.entities.Marks;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by luis on 6/04/17.
 */
public class MarksMapper implements ResultSetMapper<Marks>  {
    @Override
    public Marks map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Marks(
                resultSet.getLong("id"),
                new Data(
                        resultSet.getLong("id_data"),
                        resultSet.getString("name"),
                        resultSet.getString("description")
                )
        );
    }
}
