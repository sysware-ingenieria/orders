package com.sysware.ingenieria.entities;

/**
 * Created by luis on 6/04/17.
 */
public class Marks {
    private Long id;
    private Data data;

    public Marks(Long id, Data data) {
        this.id = id;
        this.data = data;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
