package com.sysware.ingenieria.entities;

/**
 * Created by luis on 6/04/17.
 */
public class Products {

        private Long id;
        private Data data;
        private Long id_category;
        private Long id_mark;
        private double unit_value;
        private String unit_type;

        public Products(Long id,Data data, Long id_category, Long id_mark, double unit_value, String unit_type) {
                this.id=id;
                this.data = data;
                this.id_category = id_category;
                this.id_mark = id_mark;
                this.unit_value = unit_value;
                this.unit_type = unit_type;
        }

        public Long getId() {
                return id;
        }

        public void setId(Long id) {
                this.id = id;
        }

        public Data getData() {
                return data;
        }

        public void setData(Data data) {
                this.data = data;
        }

        public Long getId_category() {
                return id_category;
        }

        public void setId_category(Long id_category) {
                this.id_category = id_category;
        }

        public Long getId_mark() {
                return id_mark;
        }

        public void setId_mark(Long id_mark) {
                this.id_mark = id_mark;
        }

        public double getUnit_value() {
                return unit_value;
        }

        public void setUnit_value(double unit_value) {
                this.unit_value = unit_value;
        }

        public String getUnit_type() {
                return unit_type;
        }

        public void setUnit_type(String unit_type) {
                this.unit_type = unit_type;
        }
}
