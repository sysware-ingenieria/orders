package com.sysware.ingenieria.entities;

/**
 * Created by luis on 6/04/17.
 */
public class PersonsOrders {
    private Long id;
    private Long id_person;
    private Long id_order;
    private String status;

    public PersonsOrders(Long id, Long id_person, Long id_order, String status) {
        this.id = id;
        this.id_person = id_person;
        this.id_order = id_order;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId_person() {
        return id_person;
    }

    public void setId_person(Long id_person) {
        this.id_person = id_person;
    }

    public Long getId_order() {
        return id_order;
    }

    public void setId_order(Long id_order) {
        this.id_order = id_order;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
