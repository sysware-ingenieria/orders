package com.sysware.ingenieria.entities;

/**
 * Created by luis on 6/04/17.
 */
public class Orders {
    private  Long id;
    private  Long id_offert;
    private  Integer number;

    public Orders(Long id, Long id_offert, Integer number) {
        this.id = id;
        this.id_offert = id_offert;
        this.number = number;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId_offert() {
        return id_offert;
    }

    public void setId_offert(Long id_offert) {
        this.id_offert = id_offert;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }
}
