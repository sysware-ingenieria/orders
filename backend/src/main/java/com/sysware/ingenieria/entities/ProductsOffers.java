package com.sysware.ingenieria.entities;

/**
 * Created by luis on 6/04/17.
 */
public class ProductsOffers {

    private  long id;
    private  long id_product;
    private  long id_offer;

    public ProductsOffers(long id, long id_product, long id_offer) {
        this.id = id;
        this.id_product = id_product;
        this.id_offer = id_offer;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId_product() {
        return id_product;
    }

    public void setId_product(long id_product) {
        this.id_product = id_product;
    }

    public long getId_offer() {
        return id_offer;
    }

    public void setId_offer(long id_offer) {
        this.id_offer = id_offer;
    }
}
