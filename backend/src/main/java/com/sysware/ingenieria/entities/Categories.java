package com.sysware.ingenieria.entities;

/**
 * Created by luis on 6/04/17.
 */
public class Categories {
    private  long id;
    private Data data;

    public Categories(long id, Data data) {
        this.id = id;
        this.data = data;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
