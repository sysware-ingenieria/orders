package com.sysware.ingenieria.entities;

import java.sql.Time;
import java.sql.Timestamp;

/**
 * Created by luis on 6/04/17.
 */
public class Offers {

    private  Long id;
    private  Data data;
    private  Integer units;
    private  double offer_value;
    private  float percent;
    private  int state;
    private Timestamp start_date;
    private Timestamp end_date;

    public Offers(Long id, Data data, Integer units, double offer_value, float percent, int state, Timestamp start_date, Timestamp end_date) {
        this.id = id;
        this.data = data;
        this.units = units;
        this.offer_value = offer_value;
        this.percent = percent;
        this.state = state;
        this.start_date = start_date;
        this.end_date = end_date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Integer getUnits() {
        return units;
    }

    public void setUnits(Integer units) {
        this.units = units;
    }

    public double getOffer_value() {
        return offer_value;
    }

    public void setOffer_value(double offer_value) {
        this.offer_value = offer_value;
    }

    public float getPercent() {
        return percent;
    }

    public void setPercent(float percent) {
        this.percent = percent;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Timestamp getStart_date() {
        return start_date;
    }

    public void setStart_date(Timestamp start_date) {
        this.start_date = start_date;
    }

    public Timestamp getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Timestamp end_date) {
        this.end_date = end_date;
    }
}
