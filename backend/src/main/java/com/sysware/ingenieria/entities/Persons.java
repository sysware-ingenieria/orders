package com.sysware.ingenieria.entities;

/**
 * Created by luis on 6/04/17.
 */
public class Persons {
        private  Long id;
        private  String first_name;
        private  String second_name;
        private  String first_last_name;
        private  String second_last_name;

    public Persons(Long id, String first_name, String second_name, String first_last_name, String second_last_name) {
        this.id = id;
        this.first_name = first_name;
        this.second_name = second_name;
        this.first_last_name = first_last_name;
        this.second_last_name = second_last_name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getSecond_name() {
        return second_name;
    }

    public void setSecond_name(String second_name) {
        this.second_name = second_name;
    }

    public String getFirst_last_name() {
        return first_last_name;
    }

    public void setFirst_last_name(String first_last_name) {
        this.first_last_name = first_last_name;
    }

    public String getSecond_last_name() {
        return second_last_name;
    }

    public void setSecond_last_name(String second_last_name) {
        this.second_last_name = second_last_name;
    }
}
