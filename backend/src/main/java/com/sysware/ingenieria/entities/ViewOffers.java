package com.sysware.ingenieria.entities;

import java.sql.Timestamp;

/**
 * Created by luis on 6/04/17.
 */
public class ViewOffers {
    private long id;
    private  Data data;
    private  Integer units;
    private  double offer_value;
    private  float percent;
    private  Integer state;
    private  Timestamp start;
    private  Timestamp end;

    public ViewOffers(long id, Data data, Integer units, double offer_value, float percent,
                      Integer state, Timestamp start, Timestamp end) {
        this.id = id;
        this.data = data;
        this.units = units;
        this.offer_value = offer_value;
        this.percent = percent;
        this.state = state;
        this.start = start;
        this.end = end;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public void setUnits(Integer units) {
        this.units = units;
    }

    public void setOffer_value(double offer_value) {
        this.offer_value = offer_value;
    }

    public void setPercent(float percent) {
        this.percent = percent;
    }

    public void setState(Integer state) {
        this.state = state;
    }


    public Integer getUnits() {
        return units;
    }

    public double getOffer_value() {
        return offer_value;
    }

    public float getPercent() {
        return percent;
    }

    public Integer getState() {
        return state;
    }

    public Timestamp getStart() {
        return start;
    }

    public void setStart(Timestamp start) {
        this.start = start;
    }

    public Timestamp getEnd() {
        return end;
    }

    public void setEnd(Timestamp end) {
        this.end = end;
    }
}
