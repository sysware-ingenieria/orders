package com.sysware.ingenieria;

import com.sysware.ingenieria.DAOs.*;
import com.sysware.ingenieria.businesses.*;
import com.sysware.ingenieria.entities.*;
import com.sysware.ingenieria.representations.ViewOffersDTO;
import com.sysware.ingenieria.resources.*;
import io.dropwizard.Application;
import io.dropwizard.forms.MultiPartBundle;
import io.dropwizard.java8.Java8Bundle;
import io.dropwizard.java8.jdbi.DBIFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.apache.commons.dbcp2.BasicDataSource;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.process.internal.RequestScoped;
import org.skife.jdbi.v2.DBI;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;
import java.util.Timer;

import static com.sysware.ingenieria.utils.constans.K.URI_BASE;


public class Orders extends Application<OrdersConfiguration> {



    public static void main(String[] args) throws Exception  {
        new Orders().run(args);

    }





    @Override
    public void initialize(Bootstrap<OrdersConfiguration> bootstrap) {

        bootstrap.addBundle(new Java8Bundle());
        bootstrap.addBundle(new MultiPartBundle());


    }
    @Override
    public void run(OrdersConfiguration configuration, Environment environment) throws Exception {
        environment.jersey().setUrlPattern(URI_BASE+"/*");

        // Enable CORS headers
        final FilterRegistration.Dynamic corsFilter =
                environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        // Configure CORS parameters
        corsFilter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        corsFilter.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
        corsFilter.setInitParameter("allowedHeaders", "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin");
        corsFilter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "OPTIONS,GET,PUT,POST,DELETE,HEAD");
        corsFilter.setInitParameter("allowCredentials", "true");

        // Add URL mapping
        corsFilter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");




        // Se estable el ambiente de coneccion con JDBI con la base de datos de MYSQL
        final DBIFactory factory = new DBIFactory();
        final DBI jdbi = factory.build(environment, configuration.getDataSourceFactory(), "mysql");

        OrdersDatabaseConfiguration routesDatabaseConfiguration = configuration.getDatabaseConfiguration();
        BasicDataSource datasource = createDataSource(routesDatabaseConfiguration);

        environment.jersey().register(new TestResource());



        // apply security on http request
        // addAuthorizationFilterAndProvider(environment,tokenBusiness);

        final CategoriesDAO categoriesDAO = jdbi.onDemand(CategoriesDAO.class);
        final CategoriesBusinesss categoriesBusinesss= new CategoriesBusinesss(categoriesDAO);

        final DataDAO dataDAO = jdbi.onDemand(DataDAO.class);
        final DataBusinesss dataBusinesss= new DataBusinesss(dataDAO);

        final MarksDAO marksDAO = jdbi.onDemand(MarksDAO.class);
        final MarksBusinesss marksBusinesss= new MarksBusinesss(marksDAO);

        final OffersDAO offersDAO = jdbi.onDemand(OffersDAO.class);
        final OffersBusinesss offersBusinesss= new OffersBusinesss(offersDAO);

        final OrdersDAO ordersDAO= jdbi.onDemand(OrdersDAO.class);
        final OrdersBusinesss ordersBusinesss= new OrdersBusinesss(ordersDAO);

        final PersonsDAO personsDAO = jdbi.onDemand(PersonsDAO.class);
        final PersonsBusinesss personsBusinesss= new PersonsBusinesss(personsDAO);

        final PersonsOrdersDAO personsOrdersDAO = jdbi.onDemand(PersonsOrdersDAO.class);
        final PersonsOrdersBusinesss personsOrdersBusinesss = new PersonsOrdersBusinesss(personsOrdersDAO);

        final ProductsDAO productsDAO = jdbi.onDemand(ProductsDAO.class);
        final ProductsBusinesss productsBusinesss = new ProductsBusinesss(productsDAO);

        final ProductsOffersDAO productsOffersDAO = jdbi.onDemand(ProductsOffersDAO.class);
        final ProductsOffersBusinesss productsOffersBusinesss = new ProductsOffersBusinesss(productsOffersDAO);

        final ViewOffersDAO viewOffersDAO = jdbi.onDemand(ViewOffersDAO.class);
        final ViewOffersBusinesss viewOffersBusinesss = new ViewOffersBusinesss(viewOffersDAO);


        final ViewProductsDAO viewProductsDAO = jdbi.onDemand(ViewProductsDAO.class);
        final ViewProductsBusinesss viewProductsBusinesss= new ViewProductsBusinesss(viewProductsDAO);










        environment.jersey().register(new TestResource());           //servicio para test
        environment.jersey().register(new CategoriesResource(categoriesBusinesss));
        environment.jersey().register(new DataResource(dataBusinesss));
        environment.jersey().register(new MarksResource(marksBusinesss));
        environment.jersey().register(new OffersResource(offersBusinesss));
        environment.jersey().register(new OrdersResource(ordersBusinesss));
        environment.jersey().register(new PersonsResource(personsBusinesss));
        environment.jersey().register(new PersonsOrdersResource(personsOrdersBusinesss));
        environment.jersey().register(new ProductsOffersResource(productsOffersBusinesss));
        environment.jersey().register(new ProductsResource(productsBusinesss));
        environment.jersey().register(new ViewOffersResource(viewOffersBusinesss));
        environment.jersey().register(new ViewProductsResource(viewProductsBusinesss));




    }
    /**
     * @Método:  Hace  la configuración para establecer conexión con las bases de datos que se
     * va a utilizar como persistencia del servidor
     * */
    private BasicDataSource createDataSource(OrdersDatabaseConfiguration ordersDatabaseConfiguration){

        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName(ordersDatabaseConfiguration.getDriverClassName());
        basicDataSource.setUrl(ordersDatabaseConfiguration.getUrl());
        basicDataSource.setUsername(ordersDatabaseConfiguration.getUsername());
        basicDataSource.setPassword(ordersDatabaseConfiguration.getPassword());
        basicDataSource.addConnectionProperty("characterEncoding", "UTF-8");

        basicDataSource.setRemoveAbandonedOnMaintenance(true);
        basicDataSource.setRemoveAbandonedOnBorrow(true);
        basicDataSource.setRemoveAbandonedTimeout(30000);

        return basicDataSource;

    }




}
