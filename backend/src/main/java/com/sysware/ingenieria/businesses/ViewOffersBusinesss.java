package com.sysware.ingenieria.businesses;

import com.sysware.ingenieria.DAOs.ViewOffersDAO;
import com.sysware.ingenieria.entities.ViewOffers;
import com.sysware.ingenieria.utils.exeptions.IException;
import fj.data.Either;

import java.util.List;

import static com.sysware.ingenieria.utils.exeptions.ManagementException.catchException;

/**
 * Created by luis on 6/04/17.
 */
public class ViewOffersBusinesss {

    private ViewOffersDAO viewOffersDAO;

    public ViewOffersBusinesss(ViewOffersDAO viewOffersDAO) {
        this.viewOffersDAO = viewOffersDAO;
    }


    public Either<IException,List<ViewOffers>>  getAllViewOfferts(){
        try {
            return Either.right(viewOffersDAO.PRODUCTS_OFFERS_LIST());

        }catch (Exception e){
            return Either.left(catchException(e));
        }
    }
}
