/*
 * Copyright (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.sysware.ingenieria.validators;

import com.sysware.ingenieria.utils.exeptions.BussinessException;
import com.sysware.ingenieria.utils.exeptions.IException;
import com.sysware.ingenieria.utils.exeptions.ManagementException;
import fj.data.Either;

import java.util.Map;

/**
 * Esta clase contruye mensajes personalizados como respuesta de servidor al cliente.
 */
public class RowAffected {


    public static Either<IException, Map<String, Long>> validOperation(String Entity, long rows_affected, long id_entity, String operation, String operation_abr) {

        if (rows_affected>0) {
            return Either.right(ManagementException.okException(operation_abr+Entity,id_entity));
        }else{
            return Either.left( new BussinessException("The "+ Entity+" could not be "+operation+". Try again later."));
        }

    }

    public static Either<IException, Map<String, String>> validOperation(String Entity, long rows_affected, String id_entity, String operation, String operation_abr) {

        if (rows_affected>0) {
            return Either.right(ManagementException.okException(operation_abr+Entity,id_entity));
        }else{
            return Either.left( new BussinessException("The "+ Entity+" could not be "+operation+". Try again later."));
        }

    }

}
