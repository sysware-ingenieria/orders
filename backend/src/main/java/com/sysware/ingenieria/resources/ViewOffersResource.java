package com.sysware.ingenieria.resources;


import com.sysware.ingenieria.businesses.ViewOffersBusinesss;
import com.sysware.ingenieria.entities.ViewOffers;
import com.sysware.ingenieria.utils.exeptions.ExceptionResponse;
import com.sysware.ingenieria.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/offers")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ViewOffersResource {

    private ViewOffersBusinesss viewOffersBusinesss;

    public ViewOffersResource(ViewOffersBusinesss viewOffersBusinesss) {
        this.viewOffersBusinesss = viewOffersBusinesss;
    }


    @GET
    public Response getAllViewOfferts( ){
        Response response;

        Either<IException, List<ViewOffers>> allViewOffertsEither = viewOffersBusinesss.getAllViewOfferts();

        if (allViewOffertsEither.isRight()){
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }
}
